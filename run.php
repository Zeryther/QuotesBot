<?php

/*
MIT License

Copyright (c) 2018 Gigadrive Group

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

if(!file_exists("config.php"))
    die("ERROR: The config file could not be found! You may need to re-download QuotesBot.\n");

require "config.php";

if(!is_dir("vendor") || !file_exists("vendor/autoload.php"))
    die("ERROR: Could not load dependencies! Please run 'composer update'.\n");

require "vendor/autoload.php";

if(!isset($twitterConsumerKey) || empty($twitterConsumerKey) || !isset($twitterConsumerSecret) || empty($twitterConsumerSecret) || !isset($twitterAccessToken) || empty($twitterAccessToken) || !isset($twitterAccessTokenSecret) || empty($twitterAccessTokenSecret))
    die("ERROR: Invalid twitter credentials.");

if(!isset($quotes) || !is_array($quotes) || count($quotes) == 0)
    die("ERROR: Failed to read quotes. Make sure you enter at least one quote!\n");

if(!file_exists("data.json"))
    die("ERROR: Failed to read data.json\n");

use Abraham\TwitterOAuth\TwitterOAuth;

try {
    $data = json_decode(file_get_contents("data.json"),true);
    $lastQuote = isset($data["lastQuote"]) ? $data["lastQuote"] : null;

    $connection = new TwitterOAuth($twitterConsumerKey,$twitterConsumerSecret,$twitterAccessToken,$twitterAccessTokenSecret);
    $content = $connection->get("account/verify_credentials");

    $quote = null;

    while($quote == null || ((count($quotes) > 1) && $quote == $lastQuote))
        $quote = $quotes[rand(0,count($quotes)-1)];

    $connection->post("statuses/update",["status" => $quote]);

    $data["lastQuote"] = $quote;

    $fp = fopen("data.json","w");
    fwrite($fp,json_encode($data));
    fclose($fp);

    die("SUCCESS: Tweet posted.\n");
} catch(Exception $e){
    die("ERROR: " . $e->getMessage() . "\n");
}