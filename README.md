# Quotes Bot by Zeryther

Quotes Bot is the PHP Script that runs the Twitter account [tfwnogf_bot](https://twitter.com/tfwnogf_bot). It can be used to tweet random "quotes" (Strings) from a list to a twitter account.

## Installation

First, clone the repository onto your hard drive.

```
git clone https://gitlab.com/Zeryther/QuotesBot.git && cd QuotesBot/
```

Next, download the necessary dependencies (make sure [composer](https://getcomposer.org/download/) is installed!)

```
composer update
```

Now, check the **config.php** file and enter your desired configurations, more details can be found within the file itself!

## Running the script

After setting up the configurations, use a **cronjob** to run the script periodically. If you're running on linux I recommend installing a cron system with the following command (can differ for every distro):

```
sudo apt-get install cron
```