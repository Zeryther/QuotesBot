<?php

# These are your application details, head to https://apps.twitter.com, create an application and fill in the details here!
$twitterConsumerKey = "";
$twitterConsumerSecret = "";
$twitterAccessToken = "";
$twitterAccessTokenSecret = "";

# This is the list of quotes that QuoteBot will tweet out.
# Make sure to leave no duplicates to avoid errors!
$quotes = [
    "This",
    "is",
    "an",
    "example",
    "list",
    "of",
    "quotes"
];